# mypostman

# Project status
![mypostman](https://img.shields.io/badge/status-active-brightgreen)

# Project Description
In this project I have tried to cover both Rest & SOAP APIs with respective methods. it uses various processes and methods of Postman to automate the API executions and validations. I have also used the features of static data-driven testing with the help of CSV and JSON file. For generating, user friendly reports and enabling the integration with CI/CD tools. I have used Newman command line along with HTML and HTML Extra report.

# Software & Dependencies :
> Postman (preferred Latest Version)

https://www.postman.com/downloads/

> Node.js (preferred LTS Version)

https://nodejs.org/en/download/prebuilt-installer

> Newman Package

https://www.npmjs.com/package/newman

> Newman html extra package

https://www.npmjs.com/package/newman-reporter-htmlextra

> JSON Export of Collections and Environment from Postman

https://learning.postman.com/docs/getting-started/importing-and-exporting/exporting-data/


## Installation
```bash
$ git clone https://gitlab.com/poojagusinge/mypostman.git
$ cd mypostman
```
## Usage
```bash
## for HTML Report
newman run {collection_name}.json -e {environment_name}.json --reporters html --reporter-html-export {file_name}.html

## for HTML Extra Report
newman run {collection_name}.json -e {environment_name}.json --reporters htmlextra --reporter-htmlextra-export {file_name}.html
```

## Contributing
Pull requests are welcome.

## License
[MIT](https://choosealicense.com/licenses/mit/)